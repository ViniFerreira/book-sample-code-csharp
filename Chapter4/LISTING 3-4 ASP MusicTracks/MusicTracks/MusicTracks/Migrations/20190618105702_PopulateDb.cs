﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicTracks.Migrations
{
    public partial class PopulateDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "MusicTrack",
                columns: new[] { "ID", "Artist", "Length", "Title" },
                values: new object[,]
                {
                    { 1, "Vini", 20, "TheGreat" },
                    { 2, "Xizhe", 20, "Noodles" },
                    { 3, "Ankurit", 20, " Parties Alone" },
                    { 4, "Tushar", 2000, "The Ice Cream Man" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DeleteData(
            //    table: "MusicTrack",
            //    keyColumn: "ID",
            //    keyValue: 1);

            //migrationBuilder.DeleteData(
            //    table: "MusicTrack",
            //    keyColumn: "ID",
            //    keyValue: 2);

            //migrationBuilder.DeleteData(
            //    table: "MusicTrack",
            //    keyColumn: "ID",
            //    keyValue: 3);

            //migrationBuilder.DeleteData(
            //    table: "MusicTrack",
            //    keyColumn: "ID",
            //    keyValue: 4);
        }
    }
}
