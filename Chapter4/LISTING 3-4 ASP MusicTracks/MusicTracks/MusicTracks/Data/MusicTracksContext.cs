﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MusicTracks.Models
{
    public class MusicTracksContext : DbContext
    {
        public MusicTracksContext (DbContextOptions<MusicTracksContext> options)
            : base(options)
        {
        }

        public DbSet<MusicTrack> MusicTrack { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<MusicTrack>().HasData(
        //        new MusicTrack(1,"Vini", "TheGreat", 20),
        //        new MusicTrack(2,"Xizhe", "Noodles", 20),
        //        new MusicTrack(3,"Ankurit", " Parties Alone", 20),
        //        new MusicTrack(4,"Tushar", "The Ice Cream Man", 2000)
        //        );
        //}
    }
}
