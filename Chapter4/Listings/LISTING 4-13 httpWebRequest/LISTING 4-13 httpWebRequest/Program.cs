﻿using System;
using System.IO;
using System.Net;

namespace LISTING_4_13_httpWebRequest
{

//The GetResponse method on an HttpWebRequest returns a WebResponse instance that describes
//the response from the server.Note that this response is not the web page itself, but an
//object that describes the response from the server. To actually read the text from the webpage
//a program must use the GetResponseStream method on the response to get a stream from
//which the webpage text can be read. Listing 4-13 shows how this works.

    class Program
    {

    //Program will wait for the web page response
    //to be generated and the response to be read.It is possible to use the WebRequest in an
    //asynchronous manner so that a program is not paused in this way.However, the programmer
    //has to create event handlers to be called when actions are completed.
        static void Main(string[] args)
        {
            WebRequest webRequest = WebRequest.Create("http://www.microsoft.com");


        //Note that the use of using around the StreamReader ensures that the input stream is closed
        //when the web page response has been read.It is important that either this stream or the
        //WebResponse instance are explicitly closed after use, as otherwise the connection will not be
        //reused and a program might run out of web connections.

            //wrapped by using as it implements IDisposable
            using (WebResponse webResponse = webRequest.GetResponse())
            using (StreamReader responseReader = new StreamReader(webResponse.GetResponseStream()))
            {
                string siteText = responseReader.ReadToEnd();
                Console.WriteLine(siteText);
            }

            Console.ReadKey();
        }
    }
}
