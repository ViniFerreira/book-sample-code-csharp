﻿using System;
using System.IO;

namespace LISTING_4_9_The_Directory_class
{
    class Program
    {
//Shows how a program can use the Directory class to create a directory, prove that
//it exists, and then delete it.Note that if a program attempts to delete a directory that is not
//empty an exception will be thrown.

        static void Main(string[] args)
        {
            Directory.CreateDirectory("TestDir");

            if (Directory.Exists("TestDir"))
                Console.WriteLine("Directory created successfully");

            Directory.Delete("TestDir");

            Console.WriteLine("Directory deleted successfully");

            Console.ReadKey();

            
        }
    }
}
