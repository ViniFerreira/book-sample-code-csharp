﻿using System;
using System.IO;
using System.Text;

namespace LISTING_4_1_Using_a_FileStream
{
    class Program
    {

//Listing 4-1 shows how a program can use a FileStream to create an output stream connected
//to a new or existing file.The program writes a block of byes to that stream.It then creates a
//new stream that is used to read the bytes from the file.The bytes to be written are obtained by
//encoding a text string.

        static void Main(string[] args)
        {
            // Writing to a file
            FileStream outputStream = new FileStream("OutputText.txt", FileMode.OpenOrCreate, FileAccess.Write);
            string outputMessageString = "Hello world";
            byte[] outputMessageBytes = Encoding.UTF8.GetBytes(outputMessageString);
            outputStream.Write(outputMessageBytes, 0, outputMessageBytes.Length);
            outputStream.Close();

            FileStream inputStream = new FileStream("OutputText.txt", FileMode.Open, FileAccess.Read);
            long fileLength = inputStream.Length;
            byte[] readBytes = new byte[fileLength];
            inputStream.Read(readBytes, 0, (int)fileLength);
            inputStream.Close();
            string readString = Encoding.UTF8.GetString(readBytes);
            Console.WriteLine("Read message: {0}", readString);
            Console.ReadKey();
        }
    }
}
